##+-------------------------------+
##| CONFIGURACION DE PINES ROUTER |
##+-------------------------------+
##
##Output  PIN  NAME  AT_COMAND bitDigMask ACTUADOR
##1       17   DIO3  D3        3          ZONA1 A
##2       11   DIO4  D4        4          ZONA1 B
##3       15   DIO5  D5        5          ZONA2 A
##4       7    DIO11 P1        11         ZONA2 B
##
##Input   PIN NAME  AT_COMAND  bitDigMask SENSOR
##1       20  DIO0  D0         0          PIR 90
##2       19  DIO1  D1         1          PIR 360 - ZONA1
##3       18  DIO2  D2         2          PIR 360 - ZONA2
##4        4  DIO12 P2         12         MAGNETICO                                 
##
##+----------------------+
##| CONFIGURACION ROUTER |
##+----------------------+
##
##PullDown:
## PR=0X151F
## PD=0X0000
##
##Frecuencia de toma de datos:
## IR= 0X01F4 (Cuando esta habilitado)
## Para acelerar se cambio a IR= 0X00C8
##
##+---------------+
##| FRAMES USADOS |
##+---------------+
##
##0X92 -> ROUTER A COORDINADOR
##
##+-------------+
##| MODO DE USO |
##+-------------+
##
##*MANUAL
##*AUTOMATICO

import time, serial

addr_MAC=8             ## # de Bytes  de addresses MAC de Xbees, Son dependientes :all_addr, v_address
##  ********************************************************************************
##  * Variables a Actualizar cuando se cambie o agreguen nuevos Xbee a la red !!!  *
##  ********************************************************************************   
n_xbee=3                 ## Numero de Xbees, Son dependientes: all_addr, dbXbee, v_act

all_addr = [[0 for j in range(addr_MAC)] for i in range(n_xbee)] 

all_addr=[
            [0x00,0x13,0xA2,0x00,0x40,0x65,0xB6,0xEB],
            [0x00,0x13,0xA2,0x00,0x40,0x8B,0x49,0x8B],
            [0x00,0x13,0xA2,0x00,0x40,0x70,0x4B,0x96]
         ]

##R1:			(PullDown)
##[0x00,0x13,0xA2,0x00,0x40,0xF3,0x94,0xE8]
##R2:			(PullUp ?)
##[0x00,0x13,0xA2,0x00,0x40,0xA6,0x2C,0xD8]
##R3:
##[0x00,0x13,0xA2,0x00,0x40,0xA6,0x2C,0xD4]
##R4:
##[0x00,0x13,0xA2,0x00,0x40,0xB5,0xA1,0x98]
##R5:
##[0x00,0x13,0xA2,0x00,0x40,0xB5,0xB8,0x36]
##R6:
##[0x00,0x13,0xA2,0x00,0x40,0xB5,0xB8,0x2B]
##R7:
##[0x00,0x13,0xA2,0x00,0x40,0xBD,0xA0,0xFF]
##R8:
##[0x00,0x13,0xA2,0x00,0x40,0x8B,0x49,0x8B]
##R9:
##[0x00,0x13,0xA2,0x00,0x40,0x65,0xB6,0xC7]
##R10:
##[0x00,0x13,0xA2,0x00,0x40,0x65,0xB6,0xEB]
##R11:
##[0x00,0x13,0xA2,0x00,0x40,0x70,0x4B,0x96]
##R12:
##[0x00,0x13,0xA2,0x00,0x40,0x92,0x04,0x07]
##R13:
##[0x00,0x13,0xA2,0x00,0x40,0x92,0x04,0x2E]


##  ********************************************************************************
v_act = [False for i in range(n_xbee)]  ##Matriz de confirmacion para "activate"
wState=True                             ##Permite lectura de Xbees, a pesar de existir desconectados
BYTE_COUNT=22                           ##Frame: 0x92
qPinsXB=8                               ##Cantidad de pines usados en c/XBee Router
ReadPins=0                              ##Almacenara la trama de datos de Xbee's Pins
v_address = [0 for i in range(addr_MAC)]##Array variable donde se carga la Direccion MAC de Xbee leido
##DataBase de Pins de Xbee
dbXbee = [[0 for j in range(qPinsXB)] for i in range(n_xbee)] 
pos=-1
##DataBase de Outputs
dbMySQL = [[0 for j in range(int(qPinsXB/2+1))] for i in range(n_xbee)] 
v_A_M = [0 for i in range(n_xbee)]      ##Vector de estados Manual o Auto de Xbees
##DataBase momentaneo representa los Outputs en Manual y/o Auto   
dbVolat = [[0 for j in range(int(qPinsXB/2))] for i in range(n_xbee)] 

##FROM RP3
##ser = serial.Serial('/dev/ttyUSB0',baudrate=9600)

##FROM PC
ser = serial.Serial('COM6',9600,timeout=0)	

##--------------------------------------------------
def readMySQL():
    global dbMySQL
    
    dbMySQL[0][0]=0; dbMySQL[0][1]=1; dbMySQL[0][2]=0; dbMySQL[0][3]=1;
    dbMySQL[1][0]=1; dbMySQL[1][1]=1; dbMySQL[1][2]=1; dbMySQL[1][3]=1;
    dbMySQL[2][0]=1; dbMySQL[2][1]=0; dbMySQL[2][2]=1; dbMySQL[2][3]=0;

    ##dbMySQL
    print('\ndbMySQL\n')
    print('\n'.join([' '.join(['{:4X}'.format(item) for item in row]) 
        for row in dbMySQL]))    

##--------------------------------------------------
def fun_A_M():  ##0(Auto) 1(Manual)
    ##Llama de MySQL xbees con Auto y Manual,
    ##cargarlos en array v_M_A

    global v_A_M, n_xbee, dbVolat, dbXbee
    
    v_A_M[0]=0;  v_A_M[1]=0;  v_A_M[2]=0;  
    for i in range(0, n_xbee):
        if (v_A_M[i]==0):   #Es Auto
            ##Logica Programable
            ##O1
            ##!I3*I1+I2*I1+I4
            ##O2
            ##!I3*I1+I2*I1+I4
            ##O3
            ##!I2*I1+I3*I1+I4
            ##O4
            ##!I2*I1+I3*I1+I4

            dbVolat[i][0]=((not dbXbee[i][2]) and (dbXbee[i][0])) or (dbXbee[i][1] and dbXbee[i][0]) or dbXbee[i][3] #O1
            dbVolat[i][1]=((not dbXbee[i][2]) and (dbXbee[i][0])) or (dbXbee[i][1] and dbXbee[i][0]) or dbXbee[i][3] #O2
            dbVolat[i][2]=((not dbXbee[i][1]) and (dbXbee[i][0])) or (dbXbee[i][2] and dbXbee[i][0]) or dbXbee[i][3] #O3            
            dbVolat[i][3]=((not dbXbee[i][1]) and (dbXbee[i][0])) or (dbXbee[i][2] and dbXbee[i][0]) or dbXbee[i][3] #O4

        if (v_A_M[i]==1):   #Es Manual
            for j in range(0, int(qPinsXB/2)):
                dbVolat[i][j]=dbMySQL[i][j]
                
    #Comentar solo para Pruebas, Que muestra dbVolat

    #dbVolat
    print('\ndbVolat\n')
    print('\n'.join([' '.join(['{:4X}'.format(item) for item in row]) 
        for row in dbVolat]))    

##--------------------------------------------------
def writeXbee():
    ##Funcion de escritura en Xbee usando "dbVolat"

    global n_xbee, qPinsXB, v_act, dbVolat
    
    for i in range(0, n_xbee):
        for j in range(0, int(qPinsXB/2)):
            ##Envia outputs de Xbee si esta CONECTADO
            if (v_act[i]==1):   
                setPinXBEE(i,j+1,dbVolat[i][j])
    #Dandole un tiempo de actualizacion de estados a los Xbees            
    time.sleep(0.1)
    
##--------------------------------------------------
def writeMySQL():
    ##Funcion de escritura en DataBase MySQL envia estado de conexion y outputs

    global n_xbee, qPinsXB, dbMySQL, v_act, dbXbee
    
    for i in range(0, n_xbee):
        for j in range(int(qPinsXB/2), qPinsXB+1):
            ##Envia 0("Xbee offline") o 1("Xbee online")
            if (j==qPinsXB):   
                dbMySQL[i][int(qPinsXB/2)]=v_act[i]
            else:
                if (v_act[i]==1):       ##Envia outputs de Xbee si esta CONECTADO
                    dbMySQL[i][int(j-qPinsXB/2)]=dbXbee[i][j]
                else:
                    if (v_A_M[i]==0):   ##Si es Auto y se desconecta
                        dbMySQL[i][int(j-qPinsXB/2)]=dbXbee[i][j]

    #Comentar solo para Pruebas, Que muestra dbMySQL

    #dbMySQL
    print('\ndbMySQL\n')
    print('\n'.join([' '.join(['{:4X}'.format(item) for item in row]) 
        for row in dbMySQL]))

##--------------------------------------------------
def setPinXBEE(nXbee,n_output,H_L):
    ##Escritura de Xbees:

    global addr_MAC, all_addr
    
    volat=0
    ser.write(bytes([0x7E]))
    ser.write(bytes([0x00]))
    ser.write(bytes([0x10]))

    ser.write(bytes([0x17]))
    ser.write(bytes([0x00]))
    checkS=0x17+0x00

    ##Enviando para el MAC del Xbee seleccionado
    for i in range(0, addr_MAC):    #Almacenar Direccion MAC usada
        volat=all_addr[nXbee][i]
        checkS=checkS+volat         #+ Checksum
        ser.write(bytes([volat]))

    ser.write(bytes([0xFF]))          #Address Zigbee unknown
    ser.write(bytes([0xFE]))
        
    ser.write(bytes([0x02]))        
    checkS=checkS+0xFF
    checkS=checkS+0xFE
    checkS=checkS+0x02   
    
    #Seleccionando Pin
    if (n_output == 1):     #D3
        ser.write(bytes([0x44]))
        ser.write(bytes([0x33]))
        checkS=checkS+0x44+0x33   

    if (n_output == 2):     #D4
        ser.write(bytes([0x44]))
        ser.write(bytes([0x34]))
        checkS=checkS+0x44+0x34

    if (n_output == 3):     #D5
        ser.write(bytes([0x44]))
        ser.write(bytes([0x35]))
        checkS=checkS+0x44+0x35

    if (n_output == 4):     #P1
        ser.write(bytes([0x50]))
        ser.write(bytes([0x31]))
        checkS=checkS+0x50+0x31 
    
    #Seleccionando Estado
    if (H_L == 1):
        volat=0x05
        checkS=checkS+volat
        ser.write(bytes([volat]))   

    if (H_L == 0):
        volat=0x04
        checkS=checkS+volat
        ser.write(bytes([volat]))
    
    #Checksum
    checkS=0xFF-(checkS&0xFF)
    ser.write(bytes([checkS]))
    
##--------------------------------------------------    
def knownXB(n_xbee,byte7,byte6,byte5,byte4,byte3,byte2,byte1,byte0):

    global all_addr

    found=0
    for i in range(0, n_xbee):
        if (byte7==all_addr[i][0] and byte6==all_addr[i][1] and byte5==all_addr[i][2] and byte4==all_addr[i][3] and byte3==all_addr[i][4] and byte2==all_addr[i][5] and byte1==all_addr[i][6] and byte0==all_addr[i][7]):
            found=1
            return i    
    if (found == 0):
        return -1   #DESCONOCIDO REGRESA -1

##--------------------------------------------------                 
def readXbee():
    ##Lectura de Xbees y actualizacion de dbXbee:

    global n_xbee, v_act, BYTE_COUNT, addr_MAC, v_address, dbXbee

    ##--------------------------------------------------------------+   
    ##Limpiar v_act y activa timer  
    for i in range(0, n_xbee):
        v_act[i]=False
    start=time.time()

    while(time.time()-start < 0.4):
        if (ser.in_waiting>=BYTE_COUNT):
            if(ser.read() == chr(0x7E).encode('ascii')):    # b'~'
                discardByte = ser.read()
                discardByte = ser.read()
                discardByte = ser.read()    # Este tiene que ser 0x92 

                for i in range(0, addr_MAC):            # Almacenar Direccion MAC usada
                    v_address[i] = ord(ser.read())
                ##Si v_address conocido,enviar confirmacion y 
                ##Activar escritura en dbXbee y cambiar celda de v_act a "TRUE"
        
                pos=knownXB(n_xbee,v_address[0],v_address[1],v_address[2],v_address[3],v_address[4],v_address[5],v_address[6],v_address[7])
                # Posicion de Xbee en Matriz dbXbee   
                if (pos != -1):                 # Si se encuentra en la Matriz dbXbee
                    v_act[pos]=True             # Leyo Xbee de posicion "pos"
                    for i in range(0, 7):       # Deshechado: Direccion ZigBee, Dig & Anal Mask, others
                        discardByte = ser.read()
                    # Lectura de estados de Xbee Pines
                    Read1 = ord(ser.read())     # Lectura "Parte1" de Pines - HEX 
                    Read2 = ord(ser.read())     # Lectura "Parte2" de Pines - HEX
                    ReadPins = (Read1*256) + Read2
           
                    # Escritura en dbXbee
                    dbXbee[pos][0] = ReadPins & 0x1;                                            # I1=dbXbee[n][0]
                    dbXbee[pos][1] = ReadPins & 0x2; dbXbee[pos][1] = dbXbee[pos][1] >> 1;      # I2=dbXbee[n][1]
                    dbXbee[pos][2] = ReadPins & 0x4; dbXbee[pos][2] = dbXbee[pos][2] >> 2;      # I3=dbXbee[n][2]
                    dbXbee[pos][3] = ReadPins & 0x1000; dbXbee[pos][3] = dbXbee[pos][3] >> 12;  # I4=dbXbee[n][3]
                    dbXbee[pos][4] = ReadPins & 0x8; dbXbee[pos][4] = dbXbee[pos][4] >> 3;      # O1=dbXbee[n][4]
                    dbXbee[pos][5] = ReadPins & 0x10; dbXbee[pos][5] = dbXbee[pos][5] >> 4;     # O2=dbXbee[n][5]
                    dbXbee[pos][6] = ReadPins & 0x20; dbXbee[pos][6] = dbXbee[pos][6] >> 5;     # O3=dbXbee[n][6]
                    dbXbee[pos][7] = ReadPins & 0x800; dbXbee[pos][7] = dbXbee[pos][7] >> 11;   # O4=dbXbee[n][7]

                    ReadPins = 0

                    discardByte = ser.read()    # Deshechado: Checksum
                else:   # Si no se encuentra en la Matriz dbXbee
                    for i in range(0, 10):      # Deshechado
                        discardByte = ser.read()
                    print("No se encontro Frame")
    #Comentar, solo para pruebas, que Xbees encontro
    print("\n-Xbee online-\n")
    for i in range(0, n_xbee):
        print(v_act[i])    
    print("\n-\n");


    #Comentar solo para Pruebas, Que muestra dbXbee

    #dbXbee
    print('\nreadXbee\n')
    print('\n'.join([' '.join(['{:4X}'.format(item) for item in row]) 
        for row in dbXbee]))   

##--------------------------------------------------                 
## main:
    
while 1:
    readMySQL()    
    ##Leer Xbee                               ---Update "dbXbee"             *Modificado 11.24
    readXbee()
    ##Leer DataBase MySQL                     ---Update "dbMySQL"            *Modificado 11.24
    readMySQL()                  ##FALTA
    ##Funcion de Auto y Manual                ---Update "dbVolat"
    fun_A_M()                    ##FALTA                                    *Modificado 10.50
    ##Funcion de escritura en Xbees usando "dbVolat"     
    writeXbee()
    #Leer Xbee                               ---Update "dbXbee"
    readXbee()
    ##Funcion de escritura en DataBase MySQL usando "dbXbee"
    writeMySQL()                 ##FALTA
    time.sleep(0.001)

##    ##Testeo Write:
##    setPinXBEE(0,1,0)
##    time.sleep(0.25)    
##    setPinXBEE(0,1,1)
##    time.sleep(0.25)

##    ##Testeo Read:
##    readXbee()
