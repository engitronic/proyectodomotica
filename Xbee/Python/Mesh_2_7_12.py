##Built by:
##  Carlos David Sotelo Pinto
##  Vrs 3.3

##+-------------------------------+
##| CONFIGURACION DE PINES ROUTER |
##+-------------------------------+
##
##Output  PIN  NAME  AT_COMAND bitDigMask ACTUADOR
##1       17   DIO3  D3        3          ZONA1 A
##2       11   DIO4  D4        4          ZONA1 B
##3       15   DIO5  D5        5          ZONA2 A
##4       7    DIO11 P1        11         ZONA2 B
##
##Input   PIN NAME  AT_COMAND  bitDigMask SENSOR
##1       20  DIO0  D0         0          PIR 90
##2       19  DIO1  D1         1          PIR 360 - ZONA1
##3       18  DIO2  D2         2          PIR 360 - ZONA2
##4        4  DIO12 P2         12         MAGNETICO                                 
##
##+----------------------+
##| CONFIGURACION ROUTER |
##+----------------------+
##
##PullDown:
## PR=0X151F
## PD=0X0000
##
##Frecuencia de toma de datos:
## IR= 0X01F4 (Cuando esta habilitado)
## Para acelerar se cambio a IR= 0X00C8
##
##+---------------+
##| FRAMES USADOS |
##+---------------+
##
##0X92 -> ROUTER A COORDINADOR
##
##+-------------+
##| MODO DE USO |
##+-------------+
##
##*MANUAL
##*AUTOMATICO

import time, serial, MySQLdb

addr_MAC=8                  ## # de Bytes  de addresses MAC de Xbees, Son dependientes :all_addr, v_address
##  ********************************************************************************
##  * Variables a Actualizar cuando se cambie o agreguen nuevos Xbee a la red !!!  *
##  ********************************************************************************   
n_xbee=3                   ## Numero de Xbees, Son dependientes: all_addr, dbXbee, v_act

all_addr = [[0 for j in range(addr_MAC)] for i in range(n_xbee)] 

all_addr=[
            [0x00,0x13,0xA2,0x00,0x40,0xF3,0x94,0xE8],
            [0x00,0x13,0xA2,0x00,0x40,0xA6,0x2C,0xD8],
            [0x00,0x13,0xA2,0x00,0x40,0xA6,0x2C,0xD4],
            [0x00,0x13,0xA2,0x00,0x40,0xB5,0xA1,0x98],
            [0x00,0x13,0xA2,0x00,0x40,0xB5,0xB8,0x36],
            [0x00,0x13,0xA2,0x00,0x40,0xB5,0xB8,0x2B],
            [0x00,0x13,0xA2,0x00,0x40,0xBD,0xA0,0xFF],
            [0x00,0x13,0xA2,0x00,0x40,0x8B,0x49,0x8B],
            [0x00,0x13,0xA2,0x00,0x40,0x65,0xB6,0xC7],
            [0x00,0x13,0xA2,0x00,0x40,0x65,0xB6,0xEB],
            [0x00,0x13,0xA2,0x00,0x40,0x70,0x4B,0x96],
            [0x00,0x13,0xA2,0x00,0x40,0x92,0x04,0x07],
            [0x00,0x13,0xA2,0x00,0x40,0x92,0x04,0x2E]
         ]

##  ********************************************************************************
#v_act = [False for i in range(n_xbee)]  ##Matriz de confirmacion para "activate"
#wState=True                             ##Permite lectura de Xbees, a pesar de existir desconectados
BYTE_COUNT=22                           ##Frame: 0x92
qPinsXB=8                               ##Cantidad de pines usados en c/XBee Router
ReadPins=0                              ##Almacenara la trama de datos de Xbee's Pins
v_address = [0 for i in range(addr_MAC)]##Array variable donde se carga la Direccion MAC de Xbee leido
##DataBase de Pins de Xbee      Outputs e Inputs
dbXbee = [[0 for j in range(qPinsXB)] for i in range(n_xbee)] 
pos=-1
##DataBase de Outputs (4 Outputs deseados, 4 Outputs de reporte, ONLINE y MODO)
dbMySQL = [[0 for j in range(int((qPinsXB/2)*2+2))] for i in range(n_xbee)] 
#Borrar?? v_A_M = [0 for i in range(n_xbee)]      ##Vector de estados Manual o Auto de Xbees
##DataBase momentaneo representa los Outputs en Manual y/o Auto   
dbVolat = [[0 for j in range(int(qPinsXB/2))] for i in range(n_xbee)] 

##SERIAL FROM RP3
##ser = serial.Serial('/dev/ttyUSB0',baudrate=9600)

##SERIAL FROM PC
ser = serial.Serial('COM6',9600,timeout=0)	

sql="laboratoriosv2"
proyecto="proyecto"
password=""

##--------------------------------------------------
def DB_to_Uc():
    ##Read MySQL
    
    global n_xbee, dbMySQL, sql, password, proyecto

    db = MySQLdb.connect(host="localhost", user="root",passwd=password, db=proyecto)

    for i in range(0, n_xbee):
        cursor=db.cursor()
        sq_i = "SELECT * FROM "+sql+" where Num = "+str(i+1)+" "
        cursor.execute(sq_i)
        for row in cursor:
            #Deseado por Usuario
            dbMySQL[i][0]=row[3]
            dbMySQL[i][1]=row[4]
            dbMySQL[i][2]=row[5]
            dbMySQL[i][3]=row[6]
            #Reporte actual
            dbMySQL[i][4]=row[7]
            dbMySQL[i][5]=row[8]
            dbMySQL[i][6]=row[9]
            dbMySQL[i][7]=row[10]
            #Online?    
            dbMySQL[i][8]=row[11]
            #Modo?      
            dbMySQL[i][9]=row[12]
    db.close()

    ##dbMySQL
    print '\nRead -> dbMySQL:'
    print '         User                Reported    Online Mode'
    print '\n'.join([' '.join(['{:4X}'.format(item) for item in row]) 
        for row in dbMySQL])

##--------------------------------------------------
def interpreter():  ##0(Auto) 1(Manual)
    ##Wanted Outputs 

    global n_xbee, dbMySQL, dbVolat, dbXbee, qPinsXB
    
    for i in range(0, n_xbee):
        if (dbMySQL[i][9]==0):      #Auto
            ##Logica Programable
            ##O1
            ##!I3*I1+I2*I1+I4
            ##O2
            ##!I3*I1+I2*I1+I4
            ##O3
            ##!I2*I1+I3*I1+I4
            ##O4
            ##!I2*I1+I3*I1+I4

            dbVolat[i][0]=((not dbXbee[i][6]) and (dbXbee[i][4])) or (dbXbee[i][5] and dbXbee[i][4]) or dbXbee[i][7] #O1
            dbVolat[i][1]=((not dbXbee[i][6]) and (dbXbee[i][4])) or (dbXbee[i][5] and dbXbee[i][4]) or dbXbee[i][7] #O2
            dbVolat[i][2]=((not dbXbee[i][5]) and (dbXbee[i][4])) or (dbXbee[i][6] and dbXbee[i][4]) or dbXbee[i][7] #O3            
            dbVolat[i][3]=((not dbXbee[i][5]) and (dbXbee[i][4])) or (dbXbee[i][6] and dbXbee[i][4]) or dbXbee[i][7] #O4

        if (dbMySQL[i][9]==1):      #Manual
            for j in range(0, int(qPinsXB/2)):
                dbVolat[i][j]=dbMySQL[i][j]
                
    #dbVolat
    print '\ndbVolat:'
    print '\n'.join([' '.join(['{:4X}'.format(item) for item in row]) 
        for row in dbVolat])

##--------------------------------------------------
def Uc_to_XB():
    ##Write Output's states in Xbees
    
    global n_xbee, qPinsXB, dbMySQL, dbVolat
    
    for i in range(0, n_xbee):
        for j in range(0, int(qPinsXB/2)):
            ##Envia outputs de Xbee si esta CONECTADO
            if (dbMySQL[i][8]==1):   
                setPinXBEE(i,j+1,dbVolat[i][j])
    
    print '\n---> Se envio Frames a Xbees <---\n'

##--------------------------------------------------
def XB_to_Uc():
    ##Read Output's states from Xbees
    
    global n_xbee, dbMySQL, BYTE_COUNT, addr_MAC, v_address, pos, dbXbee

    ##--------------------------------------------------------------+   
    ##Limpiar v_act y activa timer  
    ser.flush()
    for i in range(0, n_xbee):
        dbMySQL[i][8]=0
    start=time.time()

    while(time.time()-start < 0.5):
        if (ser.in_waiting>=BYTE_COUNT):
            if(ser.read() == chr(0x7E)):    # '~'
                discardByte = ser.read()
                discardByte = ser.read()
                discardByte = ser.read()    # Este tiene que ser 0x92 

                for i in range(0, addr_MAC):            # Almacenar Direccion MAC usada
                    v_address[i] = ord(ser.read())
                ##Si v_address conocido,enviar confirmacion y 
                ##Activar escritura en dbXbee y cambiar celda de v_act a "TRUE"
        
                pos=knownXB(n_xbee,v_address[0],v_address[1],v_address[2],v_address[3],v_address[4],v_address[5],v_address[6],v_address[7])
                # Posicion de Xbee en Matriz dbXbee   
                if (pos != -1):                 # Si se encuentra en la Matriz dbXbee
                    dbMySQL[pos][8]=1           # Leyo Xbee de posicion "pos"
                    for i in range(0, 7):       # Deshechado: Direccion ZigBee, Dig & Anal Mask, others
                        discardByte = ser.read()
                    # Lectura de estados de Xbee Pines
                    Read1 = ord(ser.read())     # Lectura "Parte1" de Pines - HEX 
                    Read2 = ord(ser.read())     # Lectura "Parte2" de Pines - HEX
                    ReadPins = (Read1*256) + Read2
           
                    # Escritura en dbXbee
                    
                    dbXbee[pos][0] = ReadPins & 0x8; dbXbee[pos][0] = dbXbee[pos][0] >> 3;      # O1=dbXbee[n][0]
                    dbXbee[pos][1] = ReadPins & 0x10; dbXbee[pos][1] = dbXbee[pos][1] >> 4;     # O2=dbXbee[n][1]
                    dbXbee[pos][2] = ReadPins & 0x20; dbXbee[pos][2] = dbXbee[pos][2] >> 5;     # O3=dbXbee[n][2]
                    dbXbee[pos][3] = ReadPins & 0x800; dbXbee[pos][3] = dbXbee[pos][3] >> 11;   # O4=dbXbee[n][3]
                    dbXbee[pos][4] = ReadPins & 0x1;                                            # I1=dbXbee[n][4]
                    dbXbee[pos][5] = ReadPins & 0x2; dbXbee[pos][5] = dbXbee[pos][5] >> 1;      # I2=dbXbee[n][5]
                    dbXbee[pos][6] = ReadPins & 0x4; dbXbee[pos][6] = dbXbee[pos][6] >> 2;      # I3=dbXbee[n][6]
                    dbXbee[pos][7] = ReadPins & 0x1000; dbXbee[pos][7] = dbXbee[pos][7] >> 12;  # I4=dbXbee[n][7]

                    ReadPins = 0

                    discardByte = ser.read()    # Deshechado: Checksum
                else:   # Si no se encuentra en la Matriz dbXbee
                    for i in range(0, 10):      # Deshechado
                        discardByte = ser.read()
                    print 'Xbee unknown or Frame corrupt'

    #dbXbee
    print '\ndbXbee:'
    print '         Outputs            Inputs'
    print '\n'.join([' '.join(['{:4X}'.format(item) for item in row]) 
        for row in dbXbee])
    
##--------------------------------------------------
def Uc_to_DB():
    ##Writing from dbMySQL to DataBase.

    global n_xbee, qPinsXB, dbMySQL, dbXbee, sql, password, proyecto

    for i in range(0, n_xbee):
        for j in range(0, int(qPinsXB/2)):
            ##Saving Xbee's reported outputs
            dbMySQL[i][int(j+qPinsXB/2)]=dbXbee[i][j]
            if (dbMySQL[i][9]==0):      ## Auto
                dbMySQL[i][j]=dbMySQL[i][int(j+qPinsXB/2)]                

    db = MySQLdb.connect(host="localhost", user="root",passwd=password, db=proyecto)
    cursor=db.cursor()
    for i in range(0, n_xbee):
        if (dbMySQL[i][9]==0):          ## Updating - Auto Mode
            sq_i = "UPDATE "+sql+" SET Manual1A = "+str(dbMySQL[i][0])+",Manual1B = "+str(dbMySQL[i][1])+",Manual2A = "+str(dbMySQL[i][2])+",Manual2B = "+str(dbMySQL[i][3])+",Reporte1A = "+str(dbMySQL[i][4])+",Reporte1B = "+str(dbMySQL[i][5])+",Reporte2A = "+str(dbMySQL[i][6])+",Reporte2B = "+str(dbMySQL[i][7])+",Online = "+str(dbMySQL[i][8])+" WHERE Num = "+str(i+1)+" "
        else:                           ## Updating - Manual Mode
            sq_i = "UPDATE "+sql+" SET Reporte1A = "+str(dbMySQL[i][4])+",Reporte1B = "+str(dbMySQL[i][5])+",Reporte2A = "+str(dbMySQL[i][6])+",Reporte2B = "+str(dbMySQL[i][7])+",Online = "+str(dbMySQL[i][8])+" WHERE Num = "+str(i+1)+" "            
        cursor.execute(sq_i)
        db.commit()
    db.close()
    
    #dbMySQL
    print '\nWrite -> dbMySQL:'
    print '         User                Reported    Online Mode'
    print '\n'.join([' '.join(['{:4X}'.format(item) for item in row]) 
        for row in dbMySQL]) 

##--------------------------------------------------
def setPinXBEE(nXbee,n_output,H_L):
    ##Escritura de Xbees:

    global addr_MAC, all_addr
    
    volat=0
    ser.write(b'\x7E')
    ser.write(b'\x00')
    ser.write(b'\x10')

    ser.write(b'\x17')
    ser.write(b'\x00')
    checkS=0x17+0x00

    ##Enviando para el MAC del Xbee seleccionado
    for i in range(0, addr_MAC):            #Almacenar Direccion MAC usada
        volat=all_addr[nXbee][i]
        checkS=checkS+volat                 #+ Checksum
        ser.write(chr(volat))   #12121

    ser.write(chr(0xFF))    #Address Zigbee unknown
    ser.write(chr(0xFE))
        
    ser.write(chr(0x02))        
    checkS=checkS+0xFF
    checkS=checkS+0xFE
    checkS=checkS+0x02   
    
    #Seleccionando Pin
    if (n_output == 1):     #D3
        ser.write(chr(0x44))
        ser.write(chr(0x33))
        checkS=checkS+0x44+0x33   

    if (n_output == 2):     #D4
        ser.write(chr(0x44))
        ser.write(chr(0x34))
        checkS=checkS+0x44+0x34

    if (n_output == 3):     #D5
        ser.write(chr(0x44))
        ser.write(chr(0x35))
        checkS=checkS+0x44+0x35

    if (n_output == 4):     #P1
        ser.write(chr(0x50))
        ser.write(chr(0x31))
        checkS=checkS+0x50+0x31 
    
    #Seleccionando Estado
    if (H_L == 1):
        volat=0x05
        checkS=checkS+volat
        ser.write(chr(volat))   

    if (H_L == 0):
        volat=0x04
        checkS=checkS+volat
        ser.write(chr(volat))
    
    #Checksum
    checkS=0xFF-(checkS&0xFF)
    ser.write(chr(checkS))
    
##--------------------------------------------------    
def knownXB(n_xbee,byte7,byte6,byte5,byte4,byte3,byte2,byte1,byte0):

    global all_addr

    found=0
    for i in range(0, n_xbee):
        if (byte7==all_addr[i][0] and byte6==all_addr[i][1] and byte5==all_addr[i][2] and byte4==all_addr[i][3] and byte3==all_addr[i][4] and byte2==all_addr[i][5] and byte1==all_addr[i][6] and byte0==all_addr[i][7]):
            found=1
            return i    
    if (found == 0):
        return -1   #DESCONOCIDO REGRESA -1

##--------------------------------------------------                 
## main:

time.sleep(2)       ## Delay de arranque
    
while 1:
    print "--------------------------------------------"
    #Updating "dbMySQL":
    DB_to_Uc()
    #Updating "dbVolat":
    interpreter()
    #Sending FRAMES:
    Uc_to_XB()
    #Updating "dbXbee" and "dbMySQL"(Row: Online):
    XB_to_Uc()
    #Updating "dbMySQL":
    Uc_to_DB()
    time.sleep(0.001)
