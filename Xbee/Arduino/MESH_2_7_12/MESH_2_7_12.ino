/*
Built by:
  Anthony Malacas Leon
  Carlos David Sotelo Pinto



  +-------------------------------+
  | CONFIGURACION DE PINES ROUTER |
  +-------------------------------+

  Output  PIN  NAME  AT_COMAND bitDigMask ACTUADOR
  1       17   DIO3  D3        3          ZONA1 A
  2       11   DIO4  D4        4          ZONA1 B
  3       15   DIO5  D5        5          ZONA2 A
  4       7    DIO11 P1        11         ZONA2 B

  Input   PIN NAME  AT_COMAND  bitDigMask SENSOR
  1       20  DIO0  D0         0          PIR 90
  2       19  DIO1  D1         1          PIR 360 - ZONA1
  3       18  DIO2  D2         2          PIR 360 - ZONA2
  4        4  DIO12 P2         12         MAGNETICO

  +----------------------+
  | CONFIGURACION ROUTER |
  +----------------------+

  PullUp:
  PR=0X151F

  Frecuencia de toma de datos:
  IR= 0X01F4 (Cuando esta habilitado)
  Para acelerar se cambio a IR= 0X00C8

  +---------------+
  | FRAMES USADOS |
  +---------------+

  0X92 -> ROUTER A COORDINADOR

  +-------------+
  | MODO DE USO |
  +-------------+

  MANUAL
  AUTOMATICO
*/

#include <TimerOne.h>
#include <Ethernet.h>
#include <SPI.h>

const int addr_MAC = 8;           //# de Bytes  de addresses MAC de Xbees, Son dependientes :all_addr, v_address
//  ********************************************************************************
//  * Variables a Actualizar cuando se cambie o agreguen nuevos Xbee a la red !!!  *
//  ********************************************************************************
const int n_xbee = 3;             // Numero de Xbees, Son dependientes: all_addr, dbXbee, v_act
const double clock_T = 400000;   // 500 ms, no bajar de 200ms!!!
const int n_xbee1 = 13;
const byte all_addr[n_xbee1][addr_MAC] = {
  {0x00, 0x13, 0xA2, 0x00, 0x40, 0xF3, 0x94, 0xE8},   //R1
  {0x00, 0x13, 0xA2, 0x00, 0x40, 0xA6, 0x2C, 0xD8},   //R2
  {0x00, 0x13, 0xA2, 0x00, 0x40, 0xA6, 0x2C, 0xD4},   //R3
  {0x00, 0x13, 0xA2, 0x00, 0x40, 0xB5, 0xA1, 0x98},   //R4
  {0x00, 0x13, 0xA2, 0x00, 0x40, 0xB5, 0xB8, 0x36},   //R5
  {0x00, 0x13, 0xA2, 0x00, 0x40, 0xB5, 0xB8, 0x2B},   //R6
  {0x00, 0x13, 0xA2, 0x00, 0x40, 0xBD, 0xA0, 0xFF},   //R7
  {0x00, 0x13, 0xA2, 0x00, 0x40, 0x8B, 0x49, 0x8B},   //R8
  {0x00, 0x13, 0xA2, 0x00, 0x40, 0x65, 0xB6, 0xC7},   //R9
  {0x00, 0x13, 0xA2, 0x00, 0x40, 0x65, 0xB6, 0xEB},   //R10
  {0x00, 0x13, 0xA2, 0x00, 0x40, 0x70, 0x4B, 0x96},   //R11
  {0x00, 0x13, 0xA2, 0x00, 0x40, 0x92, 0x04, 0x07},   //R12
  {0x00, 0x13, 0xA2, 0x00, 0x40, 0x92, 0x04, 0x2E}    //R13
};

//  ********************************************************************************
boolean wState = true;            //Permite lectura de Xbees, a pesar de existir desconectados
const int BYTE_COUNT = 22;        //Frame: 0x92
const int qPinsXB = 8;            //Cantidad de pines usados en c/XBee Router
int ReadPins = 0;                 //Almacenará trama de datos de Xbee's Pins
byte v_address[addr_MAC];         //Array variable donde se carga la Direccion MAC de Xbee leido
int dbXbee[n_xbee][qPinsXB];      //DataBase de Pins de Xbee
int pos = -1;
int dbMySQL[n_xbee][(qPinsXB / 2) * 2 + 2]; //DataBase de Outputs de Xbee y estado de conexion
int dbVolat[n_xbee][qPinsXB / 2]; //DataBase momentaneo representa los Outputs en Manual y/o Auto

// Configuracion del Ethernet Shield
byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED}; // Direccion MAC
byte ip[] = {192, 168, 0, 5};                      // Direccion IP del Arduino
byte server[] = {192, 168, 0, 4};                  // Direccion IP del servidor
EthernetClient client;


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Timer1.initialize(clock_T);
  Timer1.attachInterrupt(Stop_Reading) ;  //Timer de parada de lectura forzada
  Ethernet.begin(mac, ip);                // Inicializamos el Ethernet Shield
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.print("--------------------------------------------");
  if (client.connect(server, 80) > 0) { // Conexion con el servidor
    //Updating "dbMySQL":
    //  DB_to_Uc();
    //Updating "dbVolat":
    interpreter();
    //Sending FRAMES:
    Uc_to_XB();
    //Updating "dbXbee" and "dbMySQL"(Row: Online):
    XB_to_Uc();
    //Updating "dbMySQL":
    Uc_to_DB();
  }
  client.stop();
  client.flush();
  delay(0.001);
}

//--------------------------------------------------

void DB_to_Uc() {
  /*
      //Read MySQL

      //global n_xbee, dbMySQL, sql, password, proyecto

      db = MySQLdb.connect(host="localhost", user="root",passwd=password, db=proyecto)

      for i in range(0, n_xbee):
          cursor=db.cursor()
          sq_i = "SELECT * FROM "+sql+" where Num = "+str(i+1)+" "
          cursor.execute(sq_i)
          for row in cursor:
              #Deseado por Usuario
              dbMySQL[i][0]=row[3]
              dbMySQL[i][1]=row[4]
              dbMySQL[i][2]=row[5]
              dbMySQL[i][3]=row[6]
              #Reporte actual
              dbMySQL[i][4]=row[7]
              dbMySQL[i][5]=row[8]
              dbMySQL[i][6]=row[9]
              dbMySQL[i][7]=row[10]
              #Online?    v_act
              dbMySQL[i][8]=row[11]
              #Modo?      v_A_M
              dbMySQL[i][9]=row[12]
      db.close()

      //dbMySQL
      print '\nRead -> dbMySQL:'
      print '         User                Reported    Online Mode'
      print '\n'.join([' '.join(['{:4X}'.format(item) for item in row])
          for row in dbMySQL])*/
}
//--------------------------------------------------
void interpreter() { //0(Auto) 1(Manual)
  //Wanted Outputs

  //global n_xbee, dbMySQL, dbVolat, dbXbee, qPinsXB

  for (int i = 0; i < n_xbee; i++) {
    if (dbMySQL[i][9] == 0) {   //Auto
      //Logica Programable
      //O1
      //!I3*I1+I2*I1+I4
      //O2
      //!I3*I1+I2*I1+I4
      //O3
      //!I2*I1+I3*I1+I4
      //O4
      //!I2*I1+I3*I1+I4

      dbVolat[i][0] = (~dbXbee[i][6] & dbXbee[i][4]) | (dbXbee[i][5] & dbXbee[i][4]) | dbXbee[i][7]; //O1
      dbVolat[i][1] = (~dbXbee[i][6] & dbXbee[i][4]) | (dbXbee[i][5] & dbXbee[i][4]) | dbXbee[i][7]; //O2
      dbVolat[i][2] = (~dbXbee[i][5] & dbXbee[i][4]) | (dbXbee[i][6] & dbXbee[i][4]) | dbXbee[i][7]; //O3
      dbVolat[i][3] = (~dbXbee[i][5] & dbXbee[i][4]) | (dbXbee[i][6] & dbXbee[i][4]) | dbXbee[i][7]; //O4
    }
    if (dbMySQL[i][9] == 1) {   //Manual
      for (int j = 0; j < qPinsXB / 2; j++) {
        dbVolat[i][j] = dbMySQL[i][j];
      }
    }
  }
  //dbVolat
  Serial.print("\ndbVolat\n");
  for (int i = 0; i < n_xbee; i++) {
    for (int j = 0; j < qPinsXB / 2; j++) {
      Serial.print(dbVolat[i][j]); Serial.print("\t");
    }
    Serial.print("\n");
  }
}
//--------------------------------------------------
void Uc_to_XB() {
  //Write Output's states in Xbees

  //global n_xbee, qPinsXB, dbMySQL, dbVolat

  for (int i = 0; i < n_xbee; i++) {
    for (int j = 0; j < qPinsXB / 2; j++) {
      //Envia outputs de Xbee si esta CONECTADO
      if (dbMySQL[i][8] == 1) {
        setPinXBEE(i, j + 1, dbVolat[i][j]);
      }
    }
  }
}
//--------------------------------------------------
void XB_to_Uc() {
  //Read Output's states from Xbees

  //global n_xbee, dbMySQL, BYTE_COUNT, addr_MAC, v_address, pos, dbXbee

  //--------------------------------------------------------------+
  //Limpiar v_act y activa timer
  Serial.flush();
  for (int i = 0; i < n_xbee; i++) {
    dbMySQL[i][8] = 0;
  }
  noInterrupts(); //Desactivar timer
  wState = true;
  interrupts();

  while (wState) {
    if (Serial.available() >= BYTE_COUNT) {
      if (Serial.read() == 0x7E) {        // 7E Inicio de FRAME
        byte discardByte = Serial.read();
        discardByte = Serial.read();
        discardByte = Serial.read();    // Este tiene que ser 0x92

        for (int i = 0; i < addr_MAC; i++) {  // Almacenar Direccion MAC usada
          v_address[i] = Serial.read();
        }
        //Si v_address conocido,enviar confirmacion y
        //Activar escritura en dbXbee y cambiar celda de ONLINE (MySQL) a "1"

        pos = knownXB(n_xbee, v_address[0], v_address[1], v_address[2], v_address[3], v_address[4], v_address[5], v_address[6], v_address[7]);
        // Posicion de Xbee en Matriz dbXbee
        if (pos != -1) {                  // Si se encuentra en la Matriz dbXbee
          dbMySQL[pos][8] = 1;          // Leyo Xbee de posicion "pos"
          for (int i = 0; i < 7; i++) { // Deshechado: Direccion ZigBee, Dig & Anal Mask, others
            byte discardByte = Serial.read();
          }
          // Lectura de estados de Xbee Pines
          int Read1 = Serial.read();     // Lectura "Parte1" de Pines - HEX
          int Read2 = Serial.read();     // Lectura "Parte2" de Pines - HEX
          ReadPins = (Read1 * 256) + Read2;

          // Escritura en dbXbee
          dbXbee[pos][0] = ReadPins & 0x8; dbXbee[pos][0] = dbXbee[pos][0] >> 3;      // O1=dbXbee[n][0]
          dbXbee[pos][1] = ReadPins & 0x10; dbXbee[pos][1] = dbXbee[pos][1] >> 4;     // O2=dbXbee[n][1]
          dbXbee[pos][2] = ReadPins & 0x20; dbXbee[pos][2] = dbXbee[pos][2] >> 5;     // O3=dbXbee[n][2]
          dbXbee[pos][3] = ReadPins & 0x800; dbXbee[pos][3] = dbXbee[pos][3] >> 11;   // O4=dbXbee[n][3]
          dbXbee[pos][4] = ReadPins & 0x1;                                            // I1=dbXbee[n][4]
          dbXbee[pos][5] = ReadPins & 0x2; dbXbee[pos][5] = dbXbee[pos][5] >> 1;      // I2=dbXbee[n][5]
          dbXbee[pos][6] = ReadPins & 0x4; dbXbee[pos][6] = dbXbee[pos][6] >> 2;      // I3=dbXbee[n][6]
          dbXbee[pos][7] = ReadPins & 0x1000; dbXbee[pos][7] = dbXbee[pos][7] >> 12;  // I4=dbXbee[n][7]

          ReadPins = 0;

          byte discardByte = Serial.read();  // Deshechado: Checksum
        }
        else {         //Si no se encuentra en la Matriz dbXbee
          for (int i = 0; i < 10; i++) { // Deshechado
            byte discardByte = Serial.read();
          }
          Serial.println("Xbee unknown or Frame corrupt");
        }
      }
    }
  }
  //dbXbee
  Serial.print("\nreadXbee\n"); Serial.print("         Outputs                         Inputs\n");
  for (int i = 0; i < n_xbee; i++) {
    for (int j = 0; j < qPinsXB; j++) {
      Serial.print(dbXbee[i][j]); Serial.print("\t");
    }
    Serial.print("\n");
  }
}
//--------------------------------------------------
void Uc_to_DB() {
  //Writing from dbMySQL to PHP.

  //global n_xbee, qPinsXB, dbMySQL, dbXbee

  for (int i = 0; i < n_xbee; i++) {
    for (int j = 0; j < qPinsXB / 2; j++) {
      //Saving Xbee's reported outputs
      dbMySQL[i][int(j + qPinsXB / 2)] = dbXbee[i][j];
      if (dbMySQL[i][9] == 0) {   // Auto
        dbMySQL[i][j] = dbMySQL[i][int(j + qPinsXB / 2)];
      }
    }
  }

  String query = String("GET ") + String("/Pueba/sensar1.php?");
  client.print(query); // Enviamos los datos por GET
  for (int i = 0; i < n_xbee; i++) {
    for (int j = 0; j < (qPinsXB / 2) * 2 + 1; j++) {   //Envia Todo menos columna "Modo"
      if (dbMySQL[i][9] == 0) {         // Updating - Auto Mode
        if (i == 0 && j == 0) {
          client.print(String("dbMySQL") + String(i) + String(j) + String("="));
          client.print(dbMySQL[i][j]);
        }
        else {
          client.print(String("&dbMySQL") + String(i) + String(j) + String("="));
          client.print(dbMySQL[i][j]);
        }
      }
      else {                          // Updating - Manual Mode
        if (i > 3) {
          client.print(String("&dbMySQL") + String(i) + String(j) + String("="));
          client.print(dbMySQL[i][j]);
        }

      }
    }
  }
  client.println(" HTTP/1.0");
  client.println();

  //dbMySQL
  Serial.print("\nWrite -> dbMySQL:\n"); Serial.print("         User                       Reported            Online Mode\n");
  for (int i = 0; i < n_xbee; i++) {
    for (int j = 0; j < (qPinsXB / 2) * 2 + 2; j++) {
      Serial.print(dbMySQL[i][j]); Serial.print("\t");
    }
    Serial.print("\n");
  }
}
//--------------------------------------------------
void Stop_Reading() {
  wState = !wState;
}
//--------------------------------------------------
int knownXB(int n_xbee, byte byte7, byte byte6, byte byte5, byte byte4, byte byte3, byte byte2, byte byte1, byte byte0) {

  //global all_addr

  int found = 0;
  for (int i = 0; i < n_xbee; i++) {
    if (byte7 == all_addr[i][0] && byte6 == all_addr[i][1] && byte5 == all_addr[i][2] && byte4 == all_addr[i][3] && byte3 == all_addr[i][4] && byte2 == all_addr[i][5] && byte1 == all_addr[i][6] && byte0 == all_addr[i][7]) {
      found = 1;
      return i;
    }
  }
  if (found == 0) {
    return -1; //DESCONOCIDO REGRESA 1000
  }
}
//--------------------------------------------------
void setPinXBEE(int nXbee, int n_output, int H_L) {
  //Escritura de Xbees:

  //global addr_MAC, all_addr

  byte volat;
  Serial.write(0x7E);
  Serial.write(0x00);
  Serial.write(0x10);

  Serial.write(0x17);
  Serial.write(0x00);
  long checkS = 0x17 + 0x00;

  //Enviando para el MAC del Xbee seleccionado
  for (int i = 0; i < addr_MAC; i++) { // Almacenar Direccion MAC usada
    volat = all_addr[nXbee][i];
    checkS = checkS + volat;          // + Checksum
    Serial.write(volat);
  }

  Serial.write(0xFF);                 //Address Zigbee unknown
  Serial.write(0xFE);

  Serial.write(0x02);
  checkS = checkS + 0xFF;
  checkS = checkS + 0xFE;
  checkS = checkS + 0x02;

  //Seleccionando Pin
  if (n_output == 1) {  //D3
    Serial.write(0x44);
    Serial.write(0x33);
    checkS = checkS + 0x44 + 0x33;
  }
  if (n_output == 2) {  //D4
    Serial.write(0x44);
    Serial.write(0x34);
    checkS = checkS + 0x44 + 0x34;
  }
  if (n_output == 3) {  //D5
    Serial.write(0x44);
    Serial.write(0x35);
    checkS = checkS + 0x44 + 0x35;
  }
  if (n_output == 4) {  //P1
    Serial.write(0x50);
    Serial.write(0x31);
    checkS = checkS + 0x50 + 0x31;
  }

  //Seleccionando Estado
  if (H_L == 1) {
    volat = 0x05;
    checkS = checkS + volat;
    Serial.write(volat);
  }
  if (H_L == 0) {
    volat = 0x04;
    checkS = checkS + volat;
    Serial.write(volat);
  }

  //Checksum
  Serial.write(0xFF - (checkS & 0xFF));
}

