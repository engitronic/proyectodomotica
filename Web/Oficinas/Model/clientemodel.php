<?php
require 'config.php';
class clienteModel extends Conexion {
	var $sql;
	var $result;
	public function verificaClave() {
		$this->sql = $this->connect->prepare('select * from oficinas');
		$this->sql->execute();
		$this->result = $this->sql->fetchAll();
		// Retornando el resultado
		return $this->result;
	}
	public function nombreLaboModo($cod) {
		$this->sql = $this->connect->prepare('select * from oficinas WHERE Num=:cod');
		$this->sql->bindParam(':cod',$cod);
		$this->sql->execute();
		$this->result = $this->sql->fetchAll();
		// Retornando el resultado
		return $this->result;
	}
	public function actualizarModo($cod,$modo){
		$this->sql = $this->connect->prepare('update oficinas set Modo =:modo where Num = :cod');
		$this->sql->bindParam(':cod',$cod);
		$this->sql->bindParam(':modo',$modo);
		$this->sql->execute();
	}
	// Para las luminarias
	public function estadosLumi($cod) {
		$this->sql = $this->connect->prepare('select * from oficinas WHERE Num=:cod');
		$this->sql->bindParam(':cod',$cod);
		$this->sql->execute();
		$this->result = $this->sql->fetchAll();
		// Retornando el resultado
		return $this->result;
	}
	public function actualizarLumi1($cod,$estado){
		$this->sql = $this->connect->prepare('update oficinas set Manual1A =:estado where Num = :cod');
		$this->sql->bindParam(':cod',$cod);
		$this->sql->bindParam(':estado',$estado);
		$this->sql->execute();
	}
	public function actualizarLumi2($cod,$estado){
		$this->sql = $this->connect->prepare('update oficinas set Manual1B =:estado where Num = :cod');
		$this->sql->bindParam(':cod',$cod);
		$this->sql->bindParam(':estado',$estado);
		$this->sql->execute();
	}
	public function actualizarLumi3($cod,$estado){
		$this->sql = $this->connect->prepare('update oficinas set Manual2A =:estado where Num = :cod');
		$this->sql->bindParam(':cod',$cod);
		$this->sql->bindParam(':estado',$estado);
		$this->sql->execute();
	}
	public function actualizarLumi4($cod,$estado){
		$this->sql = $this->connect->prepare('update oficinas set Manual2B =:estado where Num = :cod');
		$this->sql->bindParam(':cod',$cod);
		$this->sql->bindParam(':estado',$estado);
		$this->sql->execute();
	}
	public function autoLumi($cod) {
		$this->sql = $this->connect->prepare('select * from oficinas WHERE Num=:cod');
		$this->sql->bindParam(':cod',$cod);
		$this->sql->execute();
		$this->result = $this->sql->fetchAll();
		// Retornando el resultado
		return $this->result;
	}
	// Para los el arduino

	public function actualizarManual($cod,$rep1,$rep2,$rep3,$rep4,$online){
		$this->sql = $this->connect->prepare('update oficinas set Reporte1A =:rep1,Reporte1B =:rep2,Reporte2A =:rep3,
		Reporte2B =:rep4,online =:online where Num = :cod');
		$this->sql->bindParam(':cod',$cod);
		$this->sql->bindParam(':rep1',$rep1);
		$this->sql->bindParam(':rep2',$rep2);
		$this->sql->bindParam(':rep3',$rep3);
		$this->sql->bindParam(':rep4',$rep4);
		$this->sql->bindParam(':online',$online);
		$this->sql->execute();
	}
	public function actualizarAuto($cod,$ma1,$ma2,$ma3,$ma4,$rep1,$rep2,$rep3,$rep4,$online){
		$this->sql = $this->connect->prepare('update oficinas set Manual1A =:ma1,Manual1B =:ma2,Manual2A =:ma3,Manual2B =:ma4,Reporte1A =:rep1,
		Reporte1B =:rep2,Reporte2A =:rep3,Reporte2B =:rep4,online =:online where Num = :cod');
		$this->sql->bindParam(':cod',$cod);
		$this->sql->bindParam(':ma1',$ma1);
		$this->sql->bindParam(':ma2',$ma2);
		$this->sql->bindParam(':ma3',$ma3);
		$this->sql->bindParam(':ma4',$ma4);
		$this->sql->bindParam(':rep1',$rep1);
		$this->sql->bindParam(':rep2',$rep2);
		$this->sql->bindParam(':rep3',$rep3);
		$this->sql->bindParam(':rep4',$rep4);
		$this->sql->bindParam(':online',$online);
		$this->sql->execute();
	}
	// oficinas
	public function listarUsuarios($clave)  {
		$this->sql = $this->connect->prepare('select * from usuarios WHERE Lugar=:clave');
		$this->sql->bindParam(':clave',$clave);
		$this->sql->execute();
		$this->result = $this->sql->fetchAll();
		// Retornando el resultado
		return $this->result;
	}
}
?>
