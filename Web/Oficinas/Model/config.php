<?php
class Conexion {
	// Variables privadas (PDO)
	private $server = "localhost";
	private $user = "root";
	private $pass = "";
	private $database = "interfazoficinas";
	protected $connect = null;
	// Declarando enl constructor
	public function Conexion() {
		try {
			$this->connect = new PDO('mysql:host='.$this->server.';dbname='.$this->database,$this->user,$this->pass, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES  \'UTF8\''));
		}
		catch(PDOException $ex){
			echo "Error al conectar a bd..!".$ex->getMessage();
		}
	}

}

?>
