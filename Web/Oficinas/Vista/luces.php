<?php
session_start();
$cod = $_SESSION['codigo'];
if($_SESSION['modo']== 0){
	header("Location: lucesa.php");
}
require '../Controler/clientecontroler.php';
$control = new clienteController();
$table = $control->estadosLumi($cod);
foreach($table as $row) {
	$esta1 = $row[3];
	$esta2 = $row[4];
	$esta3 = $row[5];
	$esta4 = $row[6];
	$repo1 = $row[7];
	$repo2 = $row[8];
	$repo3 = $row[9];
	$repo4 = $row[10];
	$estaXbee = $row[11];
}
if($esta1=="1"){
	$estado1 = "Apagar";
}
else{
	$estado1 = "Prender";
}
if($esta2=="1"){
	$estado2 = "Apagar";
}
else{
	$estado2 = "Prender";
}
if($esta3=="1"){
	$estado3 = "Apagar";
}
else{
	$estado3 = "Prender";
}
if($esta4=="1"){
	$estado4 = "Apagar";
}
else{
	$estado4 = "Prender";
}
if($repo1=="1"){
	$ima1 = "Img/on.jpg";
}
else{
	$ima1 = "Img/off.jpg";
}
if($repo2=="1"){
	$ima2 = "Img/on.jpg";
}
else{
	$ima2 = "Img/off.jpg";
}
if($repo3=="1"){
	$ima3 = "Img/on.jpg";
}
else{
	$ima3 = "Img/off.jpg";
}
if($repo4=="1"){
	$ima4 = "Img/on.jpg";
}
else{
	$ima4 = "Img/off.jpg";
}
if($estaXbee=="1"){
	$xbee = "Img/conec.png";
}
else{
	$xbee = "Img/desco.png";
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>LUMINARIAS</title>
<link rel="stylesheet" type="text/css" href="../Css/luces.css">
<script type="text/javascript" src="../Js/jquery.js"></script>
<script type="text/javascript" src="../Js/luces.js"></script>
</head>
<body>
	<div id="contenedor">
		<input type="hidden" id="led1" value="<?php echo$estado1;?>">
		<input type="hidden" id="led2" value="<?php echo$estado2;?>">
		<input type="hidden" id="led3" value="<?php echo$estado3;?>">
		<input type="hidden" id="led4" value="<?php echo$estado4;?>">
    <header>
			<h1>LUMINARIAS</h1>
		</header>
    <!--ZONA 1A-->
    <div id="zona1a">
      <h4>ZONA1 A</h4>
			<b><a href="javascript:;" onclick="Luminaria1($('#led1').val());" id="leda"><?php echo$estado1;?></a></b>
			<div id="imagen1"><img src="<?php echo$ima1;?>"></div>
		</div>
    <!--ZONA 1B-->
    <div id="zona1b">
      <h4>ZONA1 B</h4>
			<b><a href="javascript:;" onclick="Luminaria2($('#led2').val());" id="ledb"><?php echo$estado2;?></a></b>
			<div id="imagen2"><img src="<?php echo$ima2;?>"></div>
		</div>
    <!--ZONA 2A-->
    <div id="zona2a">
      <h4>ZONA2 A</h4>
			<b><a href="javascript:;" onclick="Luminaria3($('#led3').val());" id="ledc"><?php echo$estado3;?></a></b>
				<div id="imagen3"><img src="<?php echo$ima3;?>"></div>
		</div>
    <!--ZONA 2B-->
    <div id="zona2b">
      <h4>ZONA2 B</h4>
			<b><a href="javascript:;" onclick="Luminaria4($('#led4').val());" id="ledd"><?php echo$estado4;?></a></b>
			<div id="imagen4"><img src="<?php echo$ima4;?>"></div>
		</div>
  </div>
	<h4>ESTADO DEL XBEE</h5>
	<div id="estado">
		<img src=<?php echo $xbee;?>>
	</div>
  <div id="home">
		<a href="menu.php"><img src="Img/home.png"></a>
	</div>
</body>
</html>
