<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>HOME</title>
<link rel="stylesheet" type="text/css" href="../Css/usuarios.css">
</head>
<body>
  <div id="contenedor">
    <header>
        <h1>USUARIOS</h1>
    </header>
    <div id="iconos">
    <!--USUARIOS-->
      <div id="listar">
  			<h5>LISTA</h5>
  		  <a href="lista.php"><img src="Img/listar.png"></a>
  		</div>
      <!--AGREGAR-->
      <div id="agregar">
  			<h5>AGREGAR</h5>
  			<a href="agrega.php"><img src="Img/add.jpg"></a>
      </div>
      <!--EDITAR-->
      <div id="editar">
  		 	<h5>EDITAR</h5>
  		 	<a href="edita.php"><img src="Img/edit.jpg"></a>
  	 	</div>
      <!--ELIMINAR-->
      <div id="eliminar">
  			<h5>ELIMINAR</h5>
  		  <a href="elimina.php"><img src="Img/delete.jpg"></a>
  	  </div>
    </div>
  </div>
  <div id="home">
		<a href="menu.php"><img src="Img/home.png"></a>
	</div>
</body>
</html>
