<?php
  session_start();
  $clave= $_SESSION['clave'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Lista de Usuarios</title>
	<link rel="stylesheet" href="../Css/lista.css">
</head>
<body>
	<div id="contenedor">
		<div id="cabecera">
			Lista de Usuarios
		</div>
		<div id="contenido">
			<table>
        <tr>
          <th>Num</th><th>Nombres</th><th>Apellidos</th><th>Correo</th>
        </tr>
        <?php
          require '../Controler/clientecontroler.php';
          $control = new clienteController();
          $table = $control->listarUsuarios($clave);
          $i=1;
          // Recorriendo el arreglo
          foreach($table as $row) {
        ?>
        <tr><td><?php echo $i ?></td>
            <td><?php echo $row[1] ?></td>
            <td><?php echo $row[2] ?></td>
            <td><?php echo $row[4] ?></td></tr>
        <?php
          $i = $i+1;}
        ?>
      </table>
		</div>
	</div>
</body>
</html>
