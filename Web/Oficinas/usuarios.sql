-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-10-2016 a las 16:29:44
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `interfazoficinas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `Num` varchar(3) NOT NULL,
  `Nombre` varchar(20) NOT NULL,
  `Apellido` varchar(20) NOT NULL,
  `Clave` varchar(10) NOT NULL,
  `Correo` varchar(30) NOT NULL,
  `Lugar` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`Num`, `Nombre`, `Apellido`, `Clave`, `Correo`, `Lugar`) VALUES
('1', 'Luis Enrique', 'Cordoba Nuñes', 'F43B096F', 'LuisE.Cordoba@gmail.com', 'oficina1'),
('2', 'Juan Adolfo', 'Torres Palomino', '13A00801', 'Juan.Palomino@gmail.com', 'oficina2'),
('3', 'Cristian Rodrigo', 'Catro Manrique', 'A2F34510', 'Cristian.Manrique@gmail.com', 'oficina3'),
('4', 'Ezio Edson', 'Salgado Tapia', 'F43A1564', 'Edson.salgado@gmail.com', 'oficina1'),
('5', 'Erick Ernesto', 'Cipiran Gomez', '14A15D47', 'Erick.cipiran@hotmail.com', 'oficina1'),
('6', 'Nataly Ericka', 'Lopez Meza', 'A778102D', 'nataly.lopez.meza@gmail.com', 'oficina2');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`Num`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
