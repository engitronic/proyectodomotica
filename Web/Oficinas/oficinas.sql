-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-10-2016 a las 16:29:34
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `interfazoficinas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oficinas`
--

CREATE TABLE `oficinas` (
  `Num` varchar(3) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Clave` varchar(10) CHARACTER SET utf16 COLLATE utf16_spanish_ci NOT NULL,
  `Nombre` varchar(20) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Manual1A` int(1) NOT NULL,
  `Manual1B` int(1) NOT NULL,
  `Manual2A` int(1) NOT NULL,
  `Manual2B` int(1) NOT NULL,
  `Reporte1A` int(1) NOT NULL,
  `Reporte1B` int(1) NOT NULL,
  `Reporte2A` int(1) NOT NULL,
  `Reporte2B` int(1) NOT NULL,
  `Online` int(1) NOT NULL,
  `Modo` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `oficinas`
--

INSERT INTO `oficinas` (`Num`, `Clave`, `Nombre`, `Manual1A`, `Manual1B`, `Manual2A`, `Manual2B`, `Reporte1A`, `Reporte1B`, `Reporte2A`, `Reporte2B`, `Online`, `Modo`) VALUES
('1', 'oficina1', 'OFICINA 1', 1, 1, 1, 1, 0, 0, 0, 0, 0, 0),
('2', 'oficina2', 'OFICINA 2', 1, 1, 1, 1, 0, 0, 0, 0, 0, 0),
('3', 'oficina3', 'OFICINA 3', 1, 1, 1, 1, 0, 0, 0, 0, 0, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `oficinas`
--
ALTER TABLE `oficinas`
  ADD PRIMARY KEY (`Num`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
