<?php
require 'config.php';
class clienteModel extends Conexion {
	var $sql;
	var $result;
	public function verificaClave() {
		$this->sql = $this->connect->prepare('select * from laboratoriosv2');
		$this->sql->execute();
		$this->result = $this->sql->fetchAll();
		// Retornando el resultado
		return $this->result;
	}
	public function nombreLaboModo($cod) {
		$this->sql = $this->connect->prepare('select * from laboratoriosv2 WHERE Num=:cod');
		$this->sql->bindParam(':cod',$cod);
		$this->sql->execute();
		$this->result = $this->sql->fetchAll();
		// Retornando el resultado
		return $this->result;
	}
	public function actualizarModo($cod,$modo){
		$this->sql = $this->connect->prepare('update laboratoriosv2 set Modo =:modo where Num = :cod');
		$this->sql->bindParam(':cod',$cod);
		$this->sql->bindParam(':modo',$modo);
		$this->sql->execute();
	}
	// Para las luminarias
	public function estadosLumi($cod) {
		$this->sql = $this->connect->prepare('select * from laboratoriosv2 WHERE Num=:cod');
		$this->sql->bindParam(':cod',$cod);
		$this->sql->execute();
		$this->result = $this->sql->fetchAll();
		// Retornando el resultado
		return $this->result;
	}
	public function actualizarLumi1($cod,$estado){
		$this->sql = $this->connect->prepare('update laboratoriosv2 set Manual1A =:estado where Num = :cod');
		$this->sql->bindParam(':cod',$cod);
		$this->sql->bindParam(':estado',$estado);
		$this->sql->execute();
	}
	public function actualizarLumi2($cod,$estado){
		$this->sql = $this->connect->prepare('update laboratoriosv2 set Manual1B =:estado where Num = :cod');
		$this->sql->bindParam(':cod',$cod);
		$this->sql->bindParam(':estado',$estado);
		$this->sql->execute();
	}
	public function actualizarLumi3($cod,$estado){
		$this->sql = $this->connect->prepare('update laboratoriosv2 set Manual2A =:estado where Num = :cod');
		$this->sql->bindParam(':cod',$cod);
		$this->sql->bindParam(':estado',$estado);
		$this->sql->execute();
	}
	public function actualizarLumi4($cod,$estado){
		$this->sql = $this->connect->prepare('update laboratoriosv2 set Manual2B =:estado where Num = :cod');
		$this->sql->bindParam(':cod',$cod);
		$this->sql->bindParam(':estado',$estado);
		$this->sql->execute();
	}
	public function autoLumi($cod) {
		$this->sql = $this->connect->prepare('select * from laboratoriosv2 WHERE Num=:cod');
		$this->sql->bindParam(':cod',$cod);
		$this->sql->execute();
		$this->result = $this->sql->fetchAll();
		// Retornando el resultado
		return $this->result;
	}
}
?>
