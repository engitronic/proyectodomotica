-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 03, 2016 at 06:55 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `proyecto`
--

-- --------------------------------------------------------

--
-- Table structure for table `laboratoriosv2`
--

CREATE TABLE `laboratoriosv2` (
  `Num` varchar(3) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Clave` varchar(10) CHARACTER SET utf16 COLLATE utf16_spanish_ci NOT NULL,
  `Nombre` varchar(20) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Manual1A` int(1) NOT NULL,
  `Manual1B` int(1) NOT NULL,
  `Manual2A` int(1) NOT NULL,
  `Manual2B` int(1) NOT NULL,
  `Reporte1A` int(1) NOT NULL,
  `Reporte1B` int(1) NOT NULL,
  `Reporte2A` int(1) NOT NULL,
  `Reporte2B` int(1) NOT NULL,
  `Online` int(1) NOT NULL,
  `Modo` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Dumping data for table `laboratoriosv2`
--

INSERT INTO `laboratoriosv2` (`Num`, `Clave`, `Nombre`, `Manual1A`, `Manual1B`, `Manual2A`, `Manual2B`, `Reporte1A`, `Reporte1B`, `Reporte2A`, `Reporte2B`, `Online`, `Modo`) VALUES
('1', 'redesina', 'REDES INALÁMBRICAS', 1, 0, 0, 1, 0, 0, 0, 0, 0, 1),
('2', 'fibraopti', 'FIBRA ÓPTICA', 1, 1, 1, 1, 1, 1, 1, 1, 0, 0),
('3', 'telecomu', 'TELECOMUNICACIONES', 1, 0, 0, 1, 1, 0, 0, 1, 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `laboratoriosv2`
--
ALTER TABLE `laboratoriosv2`
  ADD PRIMARY KEY (`Num`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
