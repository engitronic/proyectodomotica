<?php
session_start();
$cod = $_SESSION['codigo'];
require '../Controler/clientecontroler.php';
$control = new clienteController();
$table = $control->nombreLaboModo($cod);
foreach($table as $row) {
	$nombre = $row[2];
	$modo = $row[12];
}
$_SESSION['modo'] = $modo;       // 1 Manual 0 Auto
if($modo==1){
	$mod = "Img/manu.png";
}
if($modo==0){
	$mod = "Img/auto.png";
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>HOME</title>
<link rel="stylesheet" media="screen" type="text/css" href="../Css/menu.css">
<script type="text/javascript" src="../Js/jquery.js"></script>
<script type="text/javascript" src="../Js/menu.js"></script>
</head>

<body>
	<div id="contenedor">
  	<header>
      <h1>LABORATORIO DE <?php echo $nombre; ?></h1>
    </header>
    <div id="iconos">
    	<!--LUCES-->
    	<div id="luces">
				<h5>LUMINARIAS</h5>
				<a href="luces.php"><img src="Img/luces.png"></a>
    	</div>
      <!--CARPETAS-->
      <div id="carpetas">
				<h5>CARPETAS</h5>
				<a href="#"><img src="Img/escritorio.png"></a>
			</div>
      <!--PUERTA-->
      <div id="puerta">
		 		<h5>PUERTA</h5>
		 		<a href="#"><img src="Img/puert.png"></a>
	 	 	</div>
      <!--PROYECTOR-->
      <div id="proyector">
				<h5>PROYECTOR</h5>
		  	<a href="#"><img src="Img/proyector.png"></a>
	  	</div>
      <!--PERSIANAS-->
      <div id="persianas">
				<h5>PERSIANAS</h5>
		  	<a href="#"><img src="Img/persi.png"></a>
	  	</div>
      <!--FONDO DEL PROYECTOR-->
      <div id="fondo">
				<h5>FONDO DE PROYECTOR</h5>
		  	<a href="#"><img src="Img/a.png"></a>
	 		</div>
    </div>
		<input type="hidden" id="value1" value="cambiar">
    <!--Modo-->
    <div id="modo">
			<h5>MODO DE OPERACIÓN</h5>
		  <a href="javascript:;" onclick="realizaProceso($('#value1').val());" id="modos"><img src=<?php echo $mod;?>></a>
		</div>
  </div>
</body>
</html>
